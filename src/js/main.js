const body = document.querySelector(".body");
const hamburgerButton = document.querySelector(".header__toggle");
const header = document.querySelector(".header");
const overlay = document.querySelector(".overlay");
const fadeElems = document.querySelectorAll(".has-fade");

hamburgerButton.addEventListener("click", function () {
    if (header.classList.contains("open")) {
        body.classList.remove("noscroll");
        header.classList.remove("open");
        fadeElems.forEach((element) => {
            element.classList.remove("fade-in");
            element.classList.add("fade-out");
        })
    } else {
        body.classList.add("noscroll");
        header.classList.add("open");
        fadeElems.forEach((element) => {
            element.classList.remove("fade-out");
            element.classList.add("fade-in");
        })
    }
});

window.addEventListener("resize", function () {
    if (header.classList.contains("open")) {
        body.classList.remove("noscroll");
        header.classList.remove("open");
        fadeElems.forEach((element) => {
            element.classList.remove("fade-in");
            element.classList.add("fade-out");
        })
    }
})

const masonry = new Macy({
    container: ".grid",
    mobileFirst: true,
    columns: 2,
    breakAt: {
        520:3,
        700: 4,
        1100: 8,
    },
    margin: {
        x: 10,
        y: 10
    }
})